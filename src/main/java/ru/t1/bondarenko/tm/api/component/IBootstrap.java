package ru.t1.bondarenko.tm.api.component;

public interface IBootstrap {

    void run(String... args);

}
