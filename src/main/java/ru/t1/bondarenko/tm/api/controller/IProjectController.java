package ru.t1.bondarenko.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void createProject();

    void clearProjects();

    void removeProjectByID();

    void removeProjectByIndex();

    void showProjectByID();

    void showProjectByIndex();

    void updateProjectByID();

    void updateProjectByIndex();

}
